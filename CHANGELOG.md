# CHANGELOG



## v0.2.1 (2024-04-01)

### Fix

* fix(deploy): add deploy pipeline ([`30f8c84`](https://gitlab.com/AJKF/my-package/-/commit/30f8c843fdacef227f6d9a40a8283c229466a542))


## v0.2.0 (2024-04-01)

### Feature

* feat(addition): add addition feature ([`9a264e6`](https://gitlab.com/AJKF/my-package/-/commit/9a264e6d40dd4d3ebcdb34713b186dca5c1457fd))


## v0.1.0 (2024-04-01)

### Feature

* feat(sr): add semantic release cd pipeline ([`918e010`](https://gitlab.com/AJKF/my-package/-/commit/918e0102e96b4053b36d18cfc8b9644a9a818018))

### Unknown

* add semantic_release config ([`485c03d`](https://gitlab.com/AJKF/my-package/-/commit/485c03d7ad159c05cbe2e172ce3316095fdb964b))

* Add pyproject.toml to init ([`6cefb6e`](https://gitlab.com/AJKF/my-package/-/commit/6cefb6eb0e6670ee6bee7864d26bfdbde7ece3f1))

* Update .gitlab-ci.yml file ([`fda0e6e`](https://gitlab.com/AJKF/my-package/-/commit/fda0e6ee3f7dc2c0a0b89cc77b4c77c6a0899031))

* Add first pipeline ([`191bcc7`](https://gitlab.com/AJKF/my-package/-/commit/191bcc7f1a651fc88a2fcc12d85a9885bfbab5c5))

* Initial commit ([`520880a`](https://gitlab.com/AJKF/my-package/-/commit/520880a52784f85434b8225961e25ff9d710563f))
